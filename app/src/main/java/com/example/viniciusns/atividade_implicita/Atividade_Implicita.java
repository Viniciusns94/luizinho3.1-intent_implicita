package com.example.viniciusns.atividade_implicita;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Atividade_Implicita extends AppCompatActivity {

    private final String URL = "https://www.google.com";
    private final String CHOOSER_TEXT = "Oque usar " + URL + "com";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.atividade_implicita_layout);

        Button bt = (Button) findViewById(R.id.bt_navega);

        if (bt != null) {
            bt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent baseIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(URL));
                    Intent chooserIntent = Intent.createChooser(baseIntent, CHOOSER_TEXT);
                    startActivity(chooserIntent);
                }
            });
        }
    }
}
